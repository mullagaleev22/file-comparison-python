import sys
from difflib import Differ
import threading
import time
from threading import Thread
from termcolor import cprint

def one():
    #time.sleep(5)
    thread_name = threading.current_thread().name
    print(f'Starting {thread_name}')
    with open(sys.argv[1], encoding='utf-8') as f1:
        f1_lines = f1.readlines()
        return f1_lines


def two():
    thread_name = threading.current_thread().name
    print(f'Starting {thread_name}')
    with open(sys.argv[2], encoding='utf-8') as f2:
        f2_lines = f2.readlines()
        return f2_lines


def three():
    time.sleep(1)
    if sys.argv[3] is None:
        print("2 file")
    else:
        thread_name = threading.current_thread().name
        print(f'Starting {thread_name}')
        with open(sys.argv[1], 'r') as file1:
            file1_lines = file1.readlines()
        with open(sys.argv[2], 'r') as file2:
            file2_lines = file2.readlines()
        with open(sys.argv[3], 'r') as file3:
            file3_lines = file3.readlines()
        i = 0
        for l1, l2, l3 in zip(file1_lines, file2_lines, file3_lines):
            i += 1
            if l1 == l2:
                print("Line ", i, ": IDENTICAL")
            else:
                print("Line ", i, ":")
                print("File 1:", l1, end='')
                print("File 2:", l2, end='')
                print("File 3:", l3, end='')

        file1.close()
        file2.close()
        file3.close()


Thread(target=one, name='thr-1').start()
Thread(target=two, name='thr-2').start()
Thread(target=three, name='thr-3').start()


def diff():
    d = Differ()
    difference = list(d.compare(one(), two()))
    difference = '\n'.join(difference)
    return difference


print(diff())

timeone = open(sys.argv[1], 'r')  # Check time one
lines = timeone.readlines()
t1 = lines[0]
print(t1)

timetwo = open(sys.argv[2], 'r')  # Check time two
lines = timetwo.readlines()
t2 = lines[0]
print(t2)

timethree = open(sys.argv[3], 'r')  # Check time three
lines = timethree.readlines()
t3 = lines[0]
print(t3)


def onetime1():
    with open(sys.argv[1], encoding='utf-8') as f1:
        f1.seek(19)
        f1_lines = f1.readlines()
        return f1_lines


def twotime1():
    with open(sys.argv[2], encoding='utf-8') as f2:
        f2.seek(19)
        f2_lines = f2.readlines()
        return f2_lines


if onetime1() == twotime1() and t1 != t2:
    cprint('Файлы отличаются только временем сборки', 'red', 'on_black')
else:
    cprint('разные текста', 'red', 'on_black')
